    
class Loader():
    def __init__(self):
        self.myDict = dict()
        self.myDictOGS = dict()
        self.myMax = [0,0,0,0]
        self.SIZE = 4
        
    def loadLine(self, line):
        if len(line) < self.SIZE:
            return
        myName = line[0]
        if len(myName) == 0:
            myName = line[2]
        if len(myName) == 0 or len(line[2]) == 0:
            return 0
        self.myDict[myName] = line
        self.myDictOGS[line[2]] = line
        self.myMax = [max(self.myMax[i],
                          len(line[i])) for i in range(self.SIZE)]
    def printAll(self):
        def fill(x,y):
            x = str(x)
            return x+''.join([' ']*(y-len(x)))
        
        for i in self.myDictOGS.keys():
            value = self.myDictOGS[i]
            newL = [fill(value[j],
                         self.myMax[j]+1) for j in range(self.SIZE)]
            print('|'+'|'.join(newL)+'|', end='\n')

        
if __name__ == "__main__":

    myLoader = Loader()
    with open("data/Participants.txt",'r') as myF:
        for l_ in myF:

            if len(l_) == 0:
                continue
            l = l_[:-1].split("\t")
            # for i in l:
            #     while len(i) and (l[-1]=='\n' or l[-1]==' '):
            #     i = i[:-1]
            myLoader.loadLine(l)
    myLoader.printAll()
