import sys 

def main(nameF):
    with open(nameF,'r') as f:
        for n,i_ in enumerate(f):
            if n == 0:
                print("Black - White - Result - Handicap")
                continue
            i = i_.split("(")
            if len(i) > 4:
                j = [p.split(")")[0] for p in i[1:]]
                print(j[0]+" ("+j[1]+")"+ " - "+j[2]+" ("+j[3]+")")

if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
