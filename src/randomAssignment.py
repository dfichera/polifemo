import sys
import random

if __name__ == "__main__":

    if(len(sys.argv) < 2):
        print("Usage:\n",
              sys.argv[0],
              "number of openings;",
              "Board number")
        exit(0)
    myBoardsInvolved = [int(i) for i in sys.argv[2:]]
    print ("boardsInvolved =",myBoardsInvolved)

    myBoardsInvolved = sorted(myBoardsInvolved)
    mySeed = 1
    for i in myBoardsInvolved:
        mySeed = ((mySeed +71)* i) % 73

    random.seed(mySeed)
        
    myOpening = [i+1 for i in range(int(sys.argv[1]))]
    while len(myOpening) < len(myBoardsInvolved):
        myOpening.extend(myOpening)
    random.shuffle(myOpening)
    print(["Board "+str(i)+": "+str(j) for i
           , j in zip(myBoardsInvolved,
                      myOpening[:len(myBoardsInvolved)])])
    
    
